const { app, BrowserWindow } = require('electron');
const path = require('path');
let mainWindow;

app.on('ready', () => {

  mainWindow = new BrowserWindow(
    {
      width: 556,
      height: 725,
    }
  );

  // Open devtools
  // mainWindow.webContents.openDevTools();

  mainWindow.loadURL(path.join('file://', __dirname, '../index.html'));

});
