// Empty JSON file
function cleanJSONrecord(json_file) {

  // Get the File System
  var fs = top.require('fs');

  fs.writeFile(
    location.href.slice(8, 11) + json_file,
    "[]",
    'utf-8',
    function(err)
  {
    if (err) alert(err);
  });

  return null;
}
// /cleanJSONrecord

// Refresh JSON file by deleting the old content first
function addJSONrecord(json_file, new_data) {

  // Get the File System
  var fs = top.require('fs');
	// var content_records; // Undeclared global variable

	// retrieve queue
	var json_contents = fs.readFileSync(location.href.slice(8, 11) + json_file);

	// catch empty JSON parse issue
	try {

		content_records = JSON.parse(json_contents);

	} catch(err) {
		// console.log(err);
	}

  // add new record
  content_records.push(new_data);

  fs.writeFile(
    location.href.slice(8, 11) + json_file,
    JSON.stringify(content_records),
    'utf-8',
    function(err)
  {
    if (err) alert(err);
  });

  return null;
}
// /addJSONrecord
