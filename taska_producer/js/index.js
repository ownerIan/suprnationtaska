var transaction_records = [];
var contentrecord = "";

var transactionId = "";
var userId = "";
var betAmount = 0;
var winAmount = 0;
var sessionId = "";

var startstopQueue = "";
var fileSize = 0;

// Display text file content as text
function loadFileAsText() {
	var fileToLoad = document.getElementById("fileToLoad").files[0];

	// if a file has been selected then
	if (fileToLoad != undefined) {

			var fileReader = new FileReader();
			fileReader.onload = function(fileLoadedEvent)
			{
				var textFromFileLoaded = fileLoadedEvent.target.result;
				document.getElementById("inputTextToSave").value = textFromFileLoaded;
			};
			fileReader.readAsText(fileToLoad, "UTF-8");

	}
	// /if a file has been selected then

	// refresh queue call
	cleanJSONrecord(
		"suprnation_production/suprnationtaska/queue/json/message_queue.json"
	);
	document.getElementById("parseTextToSave").value = "";
	clearInterval(startstopQueue);
	document.getElementById("parsebtninitiate").style.display = "inline";
	document.getElementById("parsebtnstart").style.display = "none";
	document.getElementById("parsebtnstop").style.display = "none";
	document.getElementById("producerloadIMG").style.display = "none";
	transaction_records = [];
}
// /loadFileAsText

// Parse text to individual records
function parseTextfile() {

	var fileToLoad = document.getElementById("fileToLoad").files[0];

	// if a file has been selected then
	if (fileToLoad != undefined) {

		var file_content = "";
		contentrecord = "";

		var fileReader = new FileReader();

		// fileLoadedEvent
		fileReader.onload = function(fileLoadedEvent)
		{
			file_content = fileLoadedEvent.target.result;
			fileSize = file_content.length;
			file_content = file_content.slice(28);

			populateRecord(file_content);
		};
		// /fileLoadedEvent

		startParse();

		fileReader.readAsText(fileToLoad, "UTF-8");

	}
	// /if a file has been selected then

}
// /parseTextfile

// Watch input file and update queue
function updateQueue() {

	// console.log("Update!");

	var fileToLoad = document.getElementById("fileToLoad").files[0];

	// if a file has been selected then
	if (fileToLoad != undefined) {

		var file_content = "";

		var fileReader = new FileReader();

		// fileLoadedEvent
		fileReader.onload = function(fileLoadedEvent)
		{
			file_content = fileLoadedEvent.target.result;
			contentrecord = "";

			// if changes were made to the text then
			if (fileSize != file_content.length) {

				var orrSize = fileSize;

				fileSize = file_content.length;
				document.getElementById("inputTextToSave").value = file_content;
				file_content = file_content.slice(orrSize+1);

				populateRecord(file_content);

			}
		};
		// /fileLoadedEvent

		fileReader.readAsText(fileToLoad, "UTF-8");

	}
	// /if a file has been selected then

}
// /updateQueue

// Populates instruction record with content
function populateRecord(filecontent) {

	// loop until all content is recorded
	do {

		transactionId = filecontent.slice(0, filecontent.search(" "));
		filecontent = filecontent.slice(filecontent.search(" ") + 1);

		userId = filecontent.slice(0, filecontent.search(" "));
		filecontent = filecontent.slice(filecontent.search(" ") + 1);

		betAmount = filecontent.slice(0, filecontent.search(" "));
		filecontent = filecontent.slice(filecontent.search(" ") + 1);

		winAmount = filecontent.slice(0, filecontent.search(" "));
		filecontent = filecontent.slice(filecontent.search(" ") + 1);

		if (filecontent.search(" ") != -1) {
			sessionId = filecontent.slice(0, filecontent.search(" "));
			filecontent = filecontent.slice(filecontent.search(" ") + 1);
		}
		else {
			sessionId = filecontent.slice(0, filecontent.length);
			filecontent = "";
		}

		contentrecord =
		'{'
		+ '"Id":' + '"' + transactionId + '"'
		+ ',"UserId":' + '"' + userId + '"'
		+ ',"Bet":' + betAmount
		+ ',"Win":' + winAmount
		+ ',"SessionId":' + '"' + sessionId + '"' +
		'}';

		contentrecord = JSON.parse(contentrecord);

		transaction_records.push(contentrecord);

		// add individual transaction
		addJSONrecord(
			"suprnation_production/suprnationtaska/queue/json/message_queue.json",
			contentrecord
		);

	}
	while (filecontent != "");

	document.getElementById("parseTextToSave").value =
		JSON.stringify(transaction_records);

}
// /populateRecord

// Start watch
function startParse() {
	startstopQueue = setInterval(function(){updateQueue()}, 3000);

	document.getElementById("parsebtninitiate").style.display = "none";
	document.getElementById("parsebtnstart").style.display = "none";
	document.getElementById("parsebtnstop").style.display = "inline";
	document.getElementById("producerloadIMG").style.display = "inline";
}
// /startParse

// Stop watch
function stopParse() {
	clearInterval(startstopQueue);

	document.getElementById("parsebtninitiate").style.display = "none";
	document.getElementById("parsebtnstop").style.display = "none";
	document.getElementById("producerloadIMG").style.display = "none";
	document.getElementById("parsebtnstart").style.display = "inline";
}
// /stopParse
