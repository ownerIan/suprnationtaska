
- Both Producer and Consumer are installed on Windows OS (preferably windows 10)
- Both Producer and Consumer assume that a folder for the queue is set as: "C:\suprnation_production\suprnationtaska\queue\json"
- if local drive is not C the application locates the local drive accordingly
- in order to expect Consumer to update in real-time, you have to have Producer switched on ('Parse text to queue' button)

A list of assumptions that need to be taken in consideration when running Producer:

- more instructions are always added to the text file and never deleted
- instructions are always added in their standard order
- an instruction is always entered complete and never partial
- null values are non-existent in the instructions (use 0)
- when a new instruction or instructions are added no extra space / tab / enter is included
- text file and the JSON file are always kept in their default location
-

A list of assumptions that need to be taken in consideration when running Consumer:
- Consumer only input is the queue
- No modification to the queue is to be done by Consumer
- Augmentation to be observed in the detailed list page (not summary page)
