
Some information on the applications:

- both Producer and Consumer have been developed with Node JS Electron technology
- the message queue is handled through a JSON file ("message_queue.json")

- BitBucket and SourceTree were used for the source control of the development. One can follow all the commits entered.
- Project url: https://ownerIan@bitbucket.org/ownerIan/suprnationtaska.git
- The project contains all the files mentioned in the below

How to install both Producer and Consumer:

- set the default directory and JSON file for the queue with local directory + "\suprnation_production\suprnationtaska\queue\json\message_queue.json" (example, "c:\suprnation_production\suprnationtaska\queue\json\message_queue.json")

- retrieve installation file (.exe).
Producer, "\suprnation_production\suprnationtaska\taska_producer\dist".
Consumer, "\suprnation_production\suprnationtaska\taska_consumer\dist".
- install both files on a windows PC

Please refer to file "assumptions.md" for more details
