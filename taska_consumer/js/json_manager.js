// To retrieve all data related to JSON File
function getJSONfile(json_file) {

  // Get the File System
  var fs = top.require('fs');
  var content_records = [];
  var json_contents = fs.readFileSync(location.href.slice(8, 11) + json_file);

  // catch empty file
  try {
    content_records = JSON.parse(json_contents);
  } catch(err) {}

  return content_records;

}
// /getJSONfile
