var startstopQueue = "";

var sumBet = 0;
var sumWin = 0;

// Updates and creates all elements for the Consumer Summary page
function updateSummary() {

  refreshSummary();
  startstopQueue = setInterval(function(){refreshSummary()}, 3000);
}
// /updateSummary

// Refresh Consumer summary priodically
function refreshSummary() {
  // console.log("tick!");

  // retrieve queue
  var JSONfile = getJSONfile(
    "suprnation_production/suprnationtaska/queue/json/message_queue.json"
  );

  var JSONfile_head = [];

  for (var i = 0; i < JSONfile.length; i++) {
    for (var key in JSONfile[i]) {
      if (JSONfile_head.indexOf(key) === -1) {
          JSONfile_head.push(key);
      }
    }
  }

  var divmainSummary = document.getElementById("dynamicConsumerSummary");
  divmainSummary.innerHTML = "";

  // loop through JSON file until all records have been summerized
  do {

    var userRecordId = "";
    var sessionRecords = [];
    var instructionRecordId = [];
    var sessionExist = false;

    userRecordId = JSONfile[0][JSONfile_head[1]];

    sessionRecords.push(
      JSON.parse(
        '{"SessionId":"' + JSONfile[0][JSONfile_head[4]] + '"'
        + ',"Bet":' + JSONfile[0][JSONfile_head[2]]
        + ',"Win":' + JSONfile[0][JSONfile_head[3]]
        +'}'
      )
    );

    JSONfile.splice(0, 1);

    // loop through all instruction records
    for (var i = 0; i < JSONfile.length; i++) {

      // if UserId match then
      if (JSONfile[i][JSONfile_head[1]] == userRecordId) {

        sessionExist = false;

        // loop through all session records
        for (var j = 0; j < sessionRecords.length; j++) {

          // if session match then
          if (sessionRecords[j].SessionId == JSONfile[i][JSONfile_head[4]]) {
            sessionRecords[j].Bet = sessionRecords[j].Bet + JSONfile[i][JSONfile_head[2]];
            sessionRecords[j].Win = sessionRecords[j].Win + JSONfile[i][JSONfile_head[3]];
            sessionExist = true;
          }
          // if no session match then
          else {
            if (sessionExist != true) {
              sessionExist = false;
            }
          }

        }
        // /loop through all session records

        if (sessionExist == false) {
          sessionRecords.push(
            JSON.parse(
              '{"SessionId":"' + JSONfile[i][JSONfile_head[4]] + '"'
              + ',"Bet":' + JSONfile[i][JSONfile_head[2]]
              + ',"Win":' + JSONfile[i][JSONfile_head[3]]
              +'}'
            )
          );
          sessionExist = true;
        }

        instructionRecordId.push(JSONfile[i][JSONfile_head[0]]);

      }
      // /if UserId match then

    }
    // //loop through all instruction records

    for (var i in instructionRecordId) {
      for (var j in JSONfile) {
        if (JSONfile[j][JSONfile_head[0]] == instructionRecordId[i]) {
          JSONfile.splice(j, 1);
        }
      }
    }

    var summaryTable = document.createElement("table"); // table
    summaryTable.setAttribute("id", "consumer_summary");

    var summaryTR = summaryTable.insertRow(-1); // table row

    for (var i = 1; i < JSONfile_head.length; i++) {

      var summaryTH = document.createElement("th"); // table header
      summaryTH.innerHTML = JSONfile_head[i];

      summaryTR.appendChild(summaryTH);

    }

    for (var i = 0; i < sessionRecords.length; i++) {

      var summaryTR = summaryTable.insertRow(-1); // table row

      var summaryTD = document.createElement("td");
      summaryTD.innerHTML = userRecordId;

      summaryTR.appendChild(summaryTD);

      var summaryTD = document.createElement("td");
      summaryTD.innerHTML = sessionRecords[i].Bet;

      summaryTR.appendChild(summaryTD);

      var summaryTD = document.createElement("td");
      summaryTD.innerHTML = sessionRecords[i].Win;

      summaryTR.appendChild(summaryTD);

      var summaryTD = document.createElement("td");
      summaryTD.innerHTML = sessionRecords[i].SessionId;

      summaryTR.appendChild(summaryTD);

    }

    var summaryUser = document.createElement("div");
    summaryUser.setAttribute("style", "float: left;");
    summaryUser.appendChild(summaryTable);

    divmainSummary.appendChild(summaryUser);

  }
  while (JSONfile != "");
  // loop through JSON file until all records have been summerized

}

// Updates and creates all elements for the Consumer List page
function updateList() {

  refreshTable();
}
// /updateList

// Populate Consumer table list
function refreshTable() {

  // retrieve queue
  var JSONfile = getJSONfile(
    "suprnation_production/suprnationtaska/queue/json/message_queue.json"
  );

  var JSONfile_head = [];

  for (var i = 0; i < JSONfile.length; i++) {
    for (var key in JSONfile[i]) {
      if (JSONfile_head.indexOf(key) === -1) {
          JSONfile_head.push(key);
      }
    }
  }

  // create dynamic input for search
  var consumer_search = document.createElement("input");
  // set input style and function
  consumer_search.setAttribute("id", "search_input");
  consumer_search.setAttribute("type", "text");
  consumer_search.setAttribute("onkeyup", "searchtable();");
  consumer_search.setAttribute("placeholder", "Search for data...");
  consumer_search.setAttribute("title", "Type in your search");

  var consumerTopTable = document.createElement("table"); // table
  consumerTopTable.setAttribute("id", "consumer_toptable");

  var consumerTR = consumerTopTable.insertRow(-1); // table row

  // create hmtl table header row using the extracted header above
  for (var i = 0; i < JSONfile_head.length; i++) {

    var consumerTH = document.createElement("th"); // table header

    if ((JSONfile_head[i] == "UserId") || (JSONfile_head[i] == "SessionId")) {

      var divfindHeader = document.createElement("div");
      divfindHeader.innerHTML = JSONfile_head[i];

      // create dynamic input for find
      var header_find = document.createElement("input");
      // set input style and function
      header_find.setAttribute("class", "findID");
      header_find.setAttribute("id", "findID" + i);
      header_find.setAttribute("type", "text");
      header_find.setAttribute("onkeyup", "findID(" + i + ");");
      header_find.setAttribute("placeholder", "#");
      header_find.setAttribute("title", "Look for ID");

      divfindHeader.appendChild(header_find);
      consumerTH.appendChild(divfindHeader);

    }
    else {
      consumerTH.innerHTML = JSONfile_head[i];
    }

    // set header border style
    if (i != JSONfile_head.length-1) {
      consumerTH.setAttribute("class", "dtable_border");
    }

    // enable the sort on header click functionality
    consumerTH.setAttribute("onclick", "sortTable(" + i + ", 'asc');");
    consumerTH.setAttribute("title", "Sort table");

    consumerTR.appendChild(consumerTH);

  }

  consumerTable = document.createElement("table"); // table
  consumerTable.setAttribute("id", "consumer_datatable");

  // add JSON data to the table as rows
  for (var i = 0; i < JSONfile.length; i++) {

    consumerTR = consumerTable.insertRow(-1); // table row

    // loop in each record for fields
    for (var j = 0; j < JSONfile_head.length; j++) {

      var consumerTD = document.createElement("td");
      consumerTD.innerHTML = JSONfile[i][JSONfile_head[j]];

      if ([JSONfile_head[j]] == "Bet") {
        sumBet = sumBet + JSONfile[i][JSONfile_head[j]];
      }

      if ([JSONfile_head[j]] == "Win") {
        sumWin = sumWin + JSONfile[i][JSONfile_head[j]];
      }

      // set border style
      if (j != JSONfile_head.length-1) {
        consumerTD.setAttribute("class", "dtable_border");
      }

      consumerTR.appendChild(consumerTD);
    }

    consumerTable.appendChild(consumerTR);
  }

  var divContainer_main = document.getElementById("dynamicConsumerList");
  divContainer_main.innerHTML = "";

  var divContainer_search = document.createElement("div");
  divContainer_search.setAttribute("id", "search_records");
  divContainer_search.appendChild(consumer_search);

  var divConsumer_top = document.createElement("div");
  divConsumer_top.setAttribute("id", "topData");
  divConsumer_top.appendChild(consumerTopTable);

  var divConsumer_table = document.createElement("div");
  divConsumer_table.setAttribute("id", "mainData");
  divConsumer_table.appendChild(consumerTable);

  var divConsumer_bottom = document.createElement("div");
  divConsumer_bottom.setAttribute("id", "bottomData");
  divConsumer_bottom.innerHTML =
    "[ Count: " + JSONfile.length + " ]"
    + "[ Total BET: " + sumBet + " ]"
    + "[ Total WIN: " + sumWin + " ]";

  divContainer_main.appendChild(divContainer_search);
  divContainer_main.appendChild(divConsumer_top);
  divContainer_main.appendChild(divConsumer_table);
  divContainer_main.appendChild(divConsumer_bottom);

}
// /refreshTable

// Sort table header functionality
function sortTable(n, dir) {
  var table, rows, switching, i, x, y, shouldSwitch, switchcount = 0;

  table = document.getElementById("consumer_datatable");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n].innerHTML.toLowerCase();
      if (x < 10) {x = ("0" + x).slice(-2)}
      y = rows[i + 1].getElementsByTagName("TD")[n].innerHTML.toLowerCase();
      if (y < 10) {y = ("0" + y).slice(-2)}
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x > y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x < y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;
    }
    else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
// /sortTable

// Go through table data and output findings
function searchtable() {

  var input, filter, table, tr, td, i;
  var totalTR = 0;

  input = document.getElementById("search_input");
  filter = input.value.toUpperCase();
  table = document.getElementById("consumer_datatable");
  tr = table.getElementsByTagName("tr");
  sumBet = 0;
  sumWin = 0;

  for (i = 0; i < tr.length; i++) {
    if (tr[i].innerHTML.toUpperCase().indexOf(">" + filter + "<") > -1) {

      tr[i].style.display = "";

      // Total BET
      sumBet = sumBet + Number(tr[i].getElementsByTagName("td")[2].innerHTML);
      // Total WIN
      sumWin = sumWin + Number(tr[i].getElementsByTagName("td")[3].innerHTML);

      // increment total Rows
      totalTR = totalTR + 1;

    } else {
      tr[i].style.display = "none";
    }
  }

  document.getElementById("bottomData").innerHTML =
    "[ Count: " + totalTR + " ]"
    + "[ Total BET: " + sumBet + " ]"
    + "[ Total WIN: " + sumWin + " ]";

}
// /searchtable

// Go through table column and output findings
function findID(colNumber) {

  var input, filter, table, tr, td, i;
  var totalTR = 0;

  input = document.getElementById("findID" + colNumber);
  filter = input.value.toUpperCase();
  table = document.getElementById("consumer_datatable");
  tr = table.getElementsByTagName("tr");
  sumBet = 0;
  sumWin = 0;

  for (i = 0; i < tr.length; i++) {
    if (
      tr[i].getElementsByTagName("td")[colNumber].innerHTML.toUpperCase().indexOf(filter) > -1
    ) {

      tr[i].style.display = "";

      // Total BET
      sumBet = sumBet + Number(tr[i].getElementsByTagName("td")[2].innerHTML);
      // Total WIN
      sumWin = sumWin + Number(tr[i].getElementsByTagName("td")[3].innerHTML);

      // increment total Rows
      totalTR = totalTR + 1;

    } else {
      tr[i].style.display = "none";
    }
  }

  // clear input field
  input.value = "";

  // update search field with result
  var searchResult = ""
  switch (colNumber) {
    case 1:
      searchResult = "Look for UserId " + filter;
      break;
    case 4:
      searchResult = "Look for SessionId " + filter;
      break;
  }
  document.getElementById("search_input").value = searchResult;

  document.getElementById("bottomData").innerHTML =
    "[ Count: " + totalTR + " ]"
    + "[ Total BET: " + sumBet + " ]"
    + "[ Total WIN: " + sumWin + " ]";

}
// /findID
