const { app, BrowserWindow } = require('electron');
const path = require('path');
let mainWindow;

app.on('ready', () => {

  mainWindow = new BrowserWindow();

  // remove default menu
  mainWindow.setMenu(null);

  // open devtools
  // mainWindow.webContents.openDevTools();

  mainWindow.loadURL(path.join('file://', __dirname, '../summary.html'));

});
